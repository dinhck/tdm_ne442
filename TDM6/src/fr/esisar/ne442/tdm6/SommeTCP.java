package fr.esisar.ne442.tdm6;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;

public class SommeTCP extends Thread {

	private final static int PORTDEB = 21000;
	private final static int PORTFIN = 23000;
	private final static int BUFFERSIZE = 2048;

	private String IP;
	private int port;

	public int result;

	public static void main(String[] args) throws InterruptedException {

		ArrayList<SommeTCP> tList = new ArrayList<>();
		int result = 0;
		int valueMax = 0;
		int portMax = 0;

		System.err.println("Creation des Thread....");
		for (int i = PORTDEB; i <= PORTFIN; i++) {
			tList.add(new SommeTCP("127.0.0.1", i));

		}
		System.err.println("Début de la recherche...");
		System.out.println("Début de la recherche...");

		for (SommeTCP t : tList)
			t.start();
		for (SommeTCP t : tList)
			t.join();

		System.err.println("Fin de la recherche...");
		System.out.println("Fin de la recherche...");

		for (SommeTCP t : tList) {
			if (t.result > valueMax) {
				valueMax = t.result;
				portMax = t.getPort();
			}
			result += t.result;
		}
		System.out.println("Le montant maximum est " + valueMax + " euros");
		System.out.println("Le port d'écoute correspondant à ce maximum est " + portMax);
		System.out.println("La somme des montants retournés par tous les port est " + result);

		System.err.println("valueMax = " + valueMax);
		System.err.println("portMax = " + portMax);
		System.err.println("result = " + result);

		System.out.println("Fin du programme");

	}

	public SommeTCP(String IP, int port) {
		this.result = 0;
		this.IP = IP;
		this.port = port;
	}

	@Override
	public void run() {
		try {
			this.getValue();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void getValue() throws IOException {
		System.err.println("Connexion au serveur....");
		InetSocketAddress add = new InetSocketAddress(this.IP, this.port);
		Socket so = new Socket();
		so.connect(add);
		byte[] send = "COMBIEN".getBytes();
		OutputStream os = so.getOutputStream();
		os.write(send);

		InputStream in = so.getInputStream();

		byte[] buffer = new byte[BUFFERSIZE];

		StringBuilder sb = new StringBuilder();
		int bufferlen;
		do {
			bufferlen = in.read(buffer);

			if (bufferlen != -1) {
				sb.append(new String(buffer, 0, bufferlen));
			}
		} while (bufferlen != -1);

		String message = sb.toString();
		System.err.println("Message reçu= " + sb.toString());

		if (message.contains("EURO") && message.contains("MONTANT=")) {
			message = message.split("=")[1];
			message = message.replace("EURO", "");

			System.err.println("Montant = " + message);

			this.result = Integer.parseInt(message);
		}
		System.err.println("Fermeture du socket....");
		so.close();

	}

	public int getPort() {
		return this.port;
	}

}

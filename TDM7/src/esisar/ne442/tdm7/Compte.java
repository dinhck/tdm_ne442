package esisar.ne442.tdm7;

import java.io.Serializable;

/*
 
* @author : Madjid KETFI
 */

import java.util.Calendar;
import java.util.Date;

/**
 * @author Madjid KETFI
 */
public class Compte implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4115571795652297891L;
	private int		code		= 0;
	private String	client		= null;
	private Date	d_ouverture	= null;
	private int		solde		= 0;

	public Compte()
	{}

	public Compte(int code, String client)
	{
		this.code = code;
		this.client = client;
		this.d_ouverture = Calendar.getInstance().getTime();
	}

	/**
	 * Consulter le compte
	 * 
	 * @return
	 */
	public int getCode()
	{
		return this.code;
	}

	/**
	 * Consulter le compte
	 * 
	 * @return
	 */
	public int consulter()
	{
		return this.solde;
	}

	/**
	 * Cr�diter le compte
	 * 
	 * @param somme
	 * @return
	 */
	public boolean crediter(int somme)
	{
		this.solde += somme;
		return true;
	}

	/**
	 * D�biter une somme de ce compte
	 * 
	 * @param somme
	 * @return
	 */
	public boolean debiter(int somme)
	{
		if (somme < this.solde)
		{
			this.solde -= somme;
			return true;
		}
		return false;
	}
	public void setCode(int code)
	{
		this.code = code;
	}
	
	public String getClient()
	{
		return this.client;
	}
	public void setClient(String client)
	{
		this.client = client;
	}
	
	public Date getDouverture()
	{
		return this.d_ouverture;
	}
	public void setClient(Date d_ouverture)
	{
		this.d_ouverture = d_ouverture;
	}
	
	public int getSolde()
	{
		return this.solde;
	}
	public void setClient(int solde)
	{
		this.solde = solde;
	}
	
	
	
}

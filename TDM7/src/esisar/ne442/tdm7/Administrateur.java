package esisar.ne442.tdm7;

/*
 * @author : Madjid KETFI 
 */

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTextField;

/**
 * @author Madjid KETFI Nom : Description :
 */
public class Administrateur extends JFrame {
	private static final long serialVersionUID = 1381505744347163446L;

	/**
	 * Cette m�thode doit permettre de s�rialiser la banque
	 */
	private void serialiserBanque() {
//		ObjectOutputStream oos = null;
//		try {
//			final FileOutputStream file = new FileOutputStream(
//					"/home/userir/TD/TDM_NE441/TDM7/OutputBanque/" + this.champSortie.getText());
//			oos = new ObjectOutputStream(file);
//			oos.writeObject(this.banque);
//			JOptionPane.showMessageDialog(this,"Sauvegarde reussi");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		try {
			final FileOutputStream file = new FileOutputStream(
					"/home/userir/TD/TDM_NE441/TDM7/OutputBanque/" + this.champSortie.getText());
			XMLEncoder encode = new XMLEncoder(file);
			encode.writeObject(this.banque);
			encode.flush();
			encode.close();
			JOptionPane.showMessageDialog(this,"Sauvegarde reussi");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cette m�thode doit permettre de d�-s�rialiser la banque
	 * 
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private void deserialiserBanque() {
//		ObjectInputStream ois = null;
//		try {
//			final FileInputStream file = new FileInputStream(
//					"/home/userir/TD/TDM_NE441/TDM7/OutputBanque/" + this.champEntree.getText());
//			ois = new ObjectInputStream(file);
//			Banque banque = (Banque) ois.readObject();
//			this.banque = banque;
//			this.automate.setBanque(banque);
//			JOptionPane.showMessageDialog(this,"Restauration reussi");
//
//		} catch (IOException | ClassNotFoundException e) {
//			e.printStackTrace();
//		}
		
		try {
			final FileInputStream file = new FileInputStream(
					"/home/userir/TD/TDM_NE441/TDM7/OutputBanque/" + this.champEntree.getText());
			XMLDecoder decode = new XMLDecoder(file);
			Banque banque = (Banque) decode.readObject();
			decode.close();
			this.banque = banque;
			this.automate.setBanque(banque);
			JOptionPane.showMessageDialog(this,"Restauration reussi");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Banque banque = null;
	private Automate automate = null;

	private javax.swing.JPanel jContentPane = null;
	private JButton boutonSerialiser = null;
	private JTextField champSortie = null;
	private JButton boutonDeserialiser = null;
	private JTextField champEntree = null;

	/**
	 * This method initializes boutonSerialiser
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getBoutonSerialiser() {
		if (boutonSerialiser == null) {
			boutonSerialiser = new JButton();
			boutonSerialiser.setBounds(14, 11, 169, 25);
			boutonSerialiser.setText("S�rialiser Banque");
			boutonSerialiser.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					serialiserBanque();
				}
			});
		}
		return boutonSerialiser;
	}

	/**
	 * This method initializes champSortie
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getChampSortie() {
		if (champSortie == null) {
			champSortie = new JTextField();
			champSortie.setBounds(15, 42, 282, 26);
		}
		return champSortie;
	}

	/**
	 * This method initializes boutonDeserialiser
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getBoutonDeserialiser() {
		if (boutonDeserialiser == null) {
			boutonDeserialiser = new JButton();
			boutonDeserialiser.setBounds(18, 92, 165, 25);
			boutonDeserialiser.setText("Restaurer Banque");
			boutonDeserialiser.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					deserialiserBanque();
				}
			});
		}
		return boutonDeserialiser;
	}

	/**
	 * This method initializes champEntree
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getChampEntree() {
		if (champEntree == null) {
			champEntree = new JTextField();
			champEntree.setBounds(19, 124, 277, 26);
		}
		return champEntree;
	}

	public static void main(String[] args) {
	}

	/**
	 * This is the default constructor
	 */
	public Administrateur(Banque banque, Automate autom) {
		super();
		this.banque = banque;
		this.automate = autom;
		initialize();
		setVisible(true);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(321, 204);
		this.setContentPane(getJContentPane());
		this.setTitle("Administrateur");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private javax.swing.JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new javax.swing.JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(getBoutonSerialiser(), null);
			jContentPane.add(getChampSortie(), null);
			jContentPane.add(getBoutonDeserialiser(), null);
			jContentPane.add(getChampEntree(), null);
		}
		return jContentPane;
	}
} // @jve:decl-index=0:visual-constraint="10,10"

package fr.esisar.ne442.tdmRevision;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Calculer extends Remote{
	void calculMoyenneGenerale(EtudiantI etu) throws RemoteException;
}

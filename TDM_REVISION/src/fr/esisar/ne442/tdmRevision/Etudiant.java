package fr.esisar.ne442.tdmRevision;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Etudiant extends UnicastRemoteObject implements EtudiantI{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1251739691156929123L;
	private int ID;
	private String name;
	private String firstName;
	private int age;
	private int promotion;
	private int moyenne;
	
	
	public int getMoyenne() {
		return moyenne;
	}
	
	@Override
	public void setMoyenne(int moyenne) throws RemoteException {
		this.moyenne = moyenne;
	}

	public int getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public String getFirstName() {
		return firstName;
	}

	public int getAge() {
		return age;
	}

	public int getPromotion() {
		return promotion;
	}
	

	public Etudiant(int ID,String name,String firstName,int age,int promotion) throws RemoteException
	{
		this.ID= ID;
		this.age = age;
		this.promotion = promotion;
		this.name = name;
		this.firstName = firstName;
	}
	
	@Override
	public String toString()
	{
		return "L'étudiant "+this.name+" "+this.firstName+"  à une moyenne de "+this.moyenne;
	}

}

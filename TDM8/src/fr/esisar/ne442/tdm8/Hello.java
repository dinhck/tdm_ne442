package fr.esisar.ne442.tdm8;

import java.rmi.Remote;
import java.rmi.RemoteException;

/***

TDM RMI NE442
Affichage du message "Hello World!"

Interface Hello
***/


public interface Hello extends Remote{
	public String sayHello(Language language) throws RemoteException;
	public String sayHello() throws RemoteException;

	enum Language
	{
		Fr,En,Sp
	}
	
}
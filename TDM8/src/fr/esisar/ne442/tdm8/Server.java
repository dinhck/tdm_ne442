package fr.esisar.ne442.tdm8;

import java.net.InetAddress;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/***
 * 
 * TDM RMI NE442 Affichage du message "Hello World!"
 * 
 * Classe Server A compléter
 ***/

//import ...

public class Server extends UnicastRemoteObject implements Hello {

	public Server() throws RemoteException {
	};

	public String sayHello(Language language) throws RemoteException{
		
		System.err.println("Appel de la methode \"sayHello(Language language)\" a distance");
		if(language ==null)
		{
			return "Hello";
		}
		switch(language)
		{
		case Fr: return "Bonjour";
		case En : return "Hello";
		case Sp : return "Holla";
		default:return "Hello";
			
		}
	}
public String sayHello() throws RemoteException{
		
		System.err.println("Appel de la methode \"sayHello()\" a distance");
		return "Hello";
	}

	public static void main(String args[]) {

		if(System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		try {
			
			// instanciation d'un objet Server
			Server server = new Server();

			// Création du registry
			Registry registry = LocateRegistry.createRegistry(1099);

			// Appel de la méthode rebind pour enregistrer l'object serveur
			String url =InetAddress.getLocalHost().getHostAddress() + "/Hello";
			System.err.println("Enregistrement RMI à l'adress " + url);
			registry.rebind(url, server);
			System.err.println("Server Ready!");
			
			
		} catch (Exception e) {
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		}
	}
}
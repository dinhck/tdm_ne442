	package fr.esisar.ne442.tdm8;

import java.rmi.Naming;
import java.rmi.Remote;

/***

TDM RMI NE442
Affichage du message "Hello World!"

Classe Client
A compléter
***/


//import ...

public class Client {
	
	private Client() {}

	public static void main(String[] args) {
        if (System.getSecurityManager() == null)
        {
        	System.setSecurityManager(new SecurityManager());
        }
		try {
            //Appel de la méthode lookup()
			
        	System.err.println("Creation du client");
        	String url = "127.0.1.1"+"/Hello";
        	System.err.println("Look up à l'adress: "+url);
			Remote r = Naming.lookup(url);
			System.err.print("Remote created : ");
			System.err.println(r);
			
			if(r instanceof Hello)
			{
				try
				{
					String hello = ((Hello)r).sayHello();					
					System.err.println("Appel de la methode hello retour: "+hello);
					System.out.println(hello);
				}catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			System.err.println("Fin client");

        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }

	}

}

package tdm2;

import java.net.InetAddress;

public class Exercice6ClientInfo {
	
	private int port ;
	private InetAddress address;
	private int ID;
	
	public Exercice6ClientInfo(int port,InetAddress adress,int ID)
	{
		this.port = port;
		this.address = adress;
		this.ID = ID;
	}

	public InetAddress getAddress() {
		return address;
	}
	public int getPort()
	{
		return this.port;
	}
	public int getID()
	{
		return this.ID;
	}

}

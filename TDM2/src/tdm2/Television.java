package tdm2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;

import javax.swing.JFrame;

public class Television {

	public static void main(String[] args) throws Exception {
		
		Television tel = new Television();
		tel.execute();
		
		
		
	}
	
	private void execute() throws Exception
	{
		
		JFrame frame = new JFrame("Television");
		frame.setSize(new Dimension(300,300));
		
		
		frame.getContentPane().setBackground(Color.GREEN);
		frame.setVisible(true);
		
		
		System.err.println("Starting udp server");

		InetSocketAddress adress = new InetSocketAddress(3000);
		DatagramSocket socket = new DatagramSocket(null);
		
		socket.bind(adress);
		
		byte[] buffer = new byte[2048];
		
		String message ;
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		System.err.println("Waiting text from NetCAT");
		
		do
		{
			socket.receive(packet);
			message = new String(buffer,packet.getOffset(),packet.getLength());
			System.err.println("packet receive, message = "+message);
			
			switch(message)
			{
			case "red\n":
				frame.getContentPane().setBackground(Color.red);
				break;
			case "green\n":
				frame.getContentPane().setBackground(Color.green);
				break;
			case "blue\n":
				frame.getContentPane().setBackground(Color.blue);
				break;
			default:
				frame.getContentPane().setBackground(Color.black);
				
			}
			
			frame.setVisible(true);
			
				
		}while(!message.contains("STOP"));
		
		frame.dispose();
		socket.close();
		
		
	
	}
		

}

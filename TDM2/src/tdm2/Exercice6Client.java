package tdm2;

import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

import javax.swing.JFrame;



public class Exercice6Client {
	
	private final static int PortDest = 3000;
	private final static String IPDest ="127.0.0.1";
	 
	private final String IP;
	private final int port;
	private final String name;
	private final boolean last;
	private int ID;
	
	
	public Exercice6Client (int portDest,String name,String IP,boolean last) throws Exception
	{
		this.name = name ;
		this.IP = IP;
		this.port = portDest;
		this.last = last;
		this.client();

		
	}

	
	private void client() throws Exception
	{
		System.err.println("Creation client "+this.name);
		
		InetSocketAddress adresseSend = new InetSocketAddress(this.IP,this.port);
		DatagramSocket socket = new DatagramSocket(null);
		
		System.err.println("client "+this.name+"\tportA: "+socket.getPort()+"\tportB: "+this.port);
		
		this.syncServer(socket, adresseSend, this.last);

		byte[] buffer = new byte[2048];
		
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		DatagramPacket packetSend;
		
		
		
		System.err.println("Creation client frame "+this.name);
		JFrame frame = new JFrame("Client: "+this.name);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setSize(new Dimension(300,300));

		
		frame.getContentPane().setBackground(Color.GREEN);
		frame.setVisible(true);
		
		String message; 
		byte[] send;
		int cnt  = 0 ;
		
		do
		{
			System.err.println("Client: "+this.name+"\tWait packet");
			socket.receive(packet);
			message = new String(buffer,packet.getOffset(),packet.getLength());
			System.err.println("Client : "+this.ID+"\tpacket receive, message = "+message);
			
			switch(message)
			{
			case "red":
				frame.getContentPane().setBackground(Color.red);
				break;
			case "green":
				frame.getContentPane().setBackground(Color.green);
				break;
			case "blue":
				frame.getContentPane().setBackground(Color.blue);
				break;
			default:
				frame.getContentPane().setBackground(Color.black);
				
			}
			
			frame.setVisible(true);
			
				
		}while(!message.contains("STOP"));
		
		socket.close();
		frame.dispose();
		
	}
	
	private void syncServer(DatagramSocket socket,InetSocketAddress address,boolean last) throws IOException
	{
		
		System.err.println("Client: "+this.name+" sync with server ...");
		String message = new String("connexion:New;last:"+last);
		
		byte [] send = message.getBytes();
		
		DatagramPacket packet = new DatagramPacket(send,send.length, address);
		
		socket.send(packet);
		byte[] receive = new byte[2048];
		 
		packet = new DatagramPacket(receive,receive.length);
		 
		socket.receive(packet);
		 
		message = new String(receive,packet.getOffset(),packet.getLength());
		 
		this.ID = Integer.parseInt(message.split(":")[1]);
		 
		System.err.println("Client: "+this.name+" successfully sync, current ID: "+this.ID);
		 
				
	}
	
}

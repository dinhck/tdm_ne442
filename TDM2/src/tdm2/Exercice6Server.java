package tdm2;

import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import javax.swing.JFrame;


public class Exercice6Server {
	
	private final static int NBCLIENTMAX = 15;
	
	private final int port; 
	private final String name;
	private ArrayList<Exercice6ClientInfo> clientConnected;
	
	public static void main(String[] args) throws Exception {
		Thread server = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try
				{
					Exercice6Server server = new Exercice6Server(3000, "Server");
					
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		server.start();
		
		ArrayList<Thread> clientL = new ArrayList<>(); 
		
		
		for(int a = 0; a <2 ; a++)
		{
			final int b = a;
			clientL.add(new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						Exercice6Client client = new Exercice6Client(3000,""+b,"127.0.0.1",false);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		}
		
		clientL.add(new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Exercice6Client client = new Exercice6Client(3000,"Last","127.0.0.1",true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));

		for (Thread t : clientL)
		{
			Thread.sleep(500);
			
			t.start();
		}

	}
	
	public Exercice6Server (int port,String name) throws Exception
	{
		this.port = port;
		this.name = name ;
		this.clientConnected = new ArrayList<>();
		this.serveur();
		
	}
	
	private void serveur() throws Exception
	{
		System.err.println("Creation server ...");
		System.err.println("Server: "+name+"\tPort: "+this.port);

		InetSocketAddress adresse = new InetSocketAddress(this.port);
		DatagramSocket socket = new DatagramSocket(null);
		socket.bind(adresse);
		
		this.syncClient(socket);
		
		DatagramPacket packet;
		String message; 
		byte[] send;
		Exercice6ClientInfo lastClient = null;
		int cnt  = 0 ;
		
		System.err.println("Server: Start roll...");
		do
		{
			cnt++;
			for(Exercice6ClientInfo client :this.clientConnected)
			{
				Thread.sleep(1000);
				if (lastClient != null)
				{
					System.err.println("Server: Change color: green\tclient: "+lastClient.getID());
					send = "green".getBytes();
					packet = new DatagramPacket(send, send.length,new InetSocketAddress(lastClient.getAddress(),lastClient.getPort()));
					socket.send(packet);		
				}
				System.err.println("Server: Change color: red\tclient: "+client.getID());
				lastClient = client;
				send = "red".getBytes();
				packet = new DatagramPacket(send, send.length,new InetSocketAddress(client.getAddress(),client.getPort()));
				socket.send(packet);	
				
				
			}			
		}while(cnt!= 100);
		
		for(Exercice6ClientInfo client :this.clientConnected)
		{
			System.err.println("Server: Stop Animation\tclient: "+client.getID());
			send = "STOP".getBytes();
			packet = new DatagramPacket(send, send.length,new InetSocketAddress(client.getAddress(),client.getPort()));
			socket.send(packet);	
			
			
		}
		
		socket.close();
	}
	private void syncClient(DatagramSocket socket) throws IOException
	{
		System.err.println("Server: Start sync client ....");
		byte send [];
		String message[];
		DatagramPacket packet ;
		byte[] buffer = new byte[2048];
		boolean last = false;
		do
		{
			packet = new DatagramPacket(buffer, buffer.length);
			socket.receive(packet);
			message = new String(buffer,packet.getOffset(),packet.getLength()).split(";");
			System.err.println("Server: receive DATA \tIP: "+packet.getAddress()+"\tPort: "+packet.getPort());
			if(message[0].split(":")[1].equals("New"))
			{
				last = Boolean.parseBoolean(message[1].split(":")[1]);
				Exercice6ClientInfo client = new Exercice6ClientInfo(packet.getPort(), packet.getAddress(),this.clientConnected.size());
				send = ("ID:"+(this.clientConnected.size())).getBytes();
				this.clientConnected.add(client);
				packet = new DatagramPacket(send, send.length,new InetSocketAddress(client.getAddress(),client.getPort()));
				
				socket.send(packet);
				
				send = "green".getBytes();
				packet = new DatagramPacket(send, send.length,new InetSocketAddress(client.getAddress(),client.getPort()));
				socket.send(packet);
				
			}
		}while(clientConnected.size()<this.NBCLIENTMAX && !last );
		
		System.err.println("Server: END sync client");
		
		
	}
	
}

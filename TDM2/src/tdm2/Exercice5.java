package tdm2;

import java.awt.Color;
import java.awt.Dimension;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import javax.swing.JFrame;


public class Exercice5 {
	
	private final static int PORTStart = 30000;
	private final static int nbClient = 6;
	
	private final int portA; 
	private final int portB;
	private final String name;
	
	public static void main(String[] args) throws Exception {
		
		Thread server = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Exercice5 serveur = new Exercice5(PORTStart,PORTStart+1,"Server 1");
					serveur.serveur();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
		
		ArrayList<Thread> clientL = new ArrayList<>(); 
		
		
		for(int a = 0; a <nbClient ; a++)
		{
			final int b = a;
			clientL.add(new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						Exercice5 client = new Exercice5(1+b+PORTStart,((2+b)%(nbClient+1))+PORTStart,""+b);
						client.client();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		}
		
		server.start();
		for (Thread t : clientL)
		{
			
			t.start();
		}
		
	}
	
	public Exercice5 (int portA,int portB,String name)
	{
		this.portB = portB;
		this.portA = portA;
		this.name = name ;
		
	}
	
	private void serveur() throws Exception
	{
		System.err.println("Creation server ...");
		System.err.println("server "+name+"\tportA: "+portA+"\tportB: "+portB);

		InetSocketAddress adresse = new InetSocketAddress(portA);
		InetSocketAddress adresseSend = new InetSocketAddress("127.0.0.1",portB);
		DatagramSocket socket = new DatagramSocket(null);
		socket.bind(adresse);
		
		byte[] buffer = new byte[2048];
		
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		DatagramPacket packetSend;
		
		
		
		System.err.println("Creation frame "+name);
		JFrame frame = new JFrame("Programme"+name);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(300,300));
		
		frame.getContentPane().setBackground(Color.red);
		frame.setVisible(true);
		
		String message; 
		byte[] send;
		int cnt  = 0 ;
		
		do
		{
			cnt++;
			System.err.println("programme: "+name+"\tWait 1S Server");
			Thread.sleep(1000);
			frame.getContentPane().setBackground(Color.GREEN);
			frame.setVisible(true);
			
			send = "red".getBytes();
			packetSend= new DatagramPacket(send, send.length,adresseSend);
			socket.send(packetSend);
			System.err.println("programme: "+name+"\tWait packet");
			socket.receive(packet);
			
			message = new String(buffer,packet.getOffset(),packet.getLength());

			if (message.equals("red"))
			{
				frame.getContentPane().setBackground(Color.red);
				frame.setVisible(true);
			}
				
		}while(cnt!= 10);
		
		socket.close();
		frame.dispose();
		
		
		
	}
	
	private void client() throws Exception
	{
		System.err.println("Creation client "+name);
		System.err.println("client "+name+"\tportA: "+portA+"\tportB: "+portB);
		InetSocketAddress adresse = new InetSocketAddress(portA);
		InetSocketAddress adresseSend = new InetSocketAddress("127.0.0.1",portB);
		DatagramSocket socket = new DatagramSocket(null);
		socket.bind(adresse);
		
		byte[] buffer = new byte[2048];
		
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		DatagramPacket packetSend;
		
		
		System.err.println("Creation client frame "+name);
		JFrame frame = new JFrame("Programme: "+name);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setSize(new Dimension(300,300));

		
		frame.getContentPane().setBackground(Color.GREEN);
		frame.setVisible(true);
		
		String message; 
		byte[] send;
		int cnt  = 0 ;
		
		do
		{
			System.err.println("programme: "+name+"\tWait packet");
			cnt++;
			socket.receive(packet);
			message = new String(buffer,packet.getOffset(),packet.getLength());

			if (message.equals("red"))
			{
				frame.getContentPane().setBackground(Color.red);
				frame.setVisible(true);
			}	
			
			Thread.sleep(1000);
			
			send = "red".getBytes();
			packetSend= new DatagramPacket(send, send.length,adresseSend);
			socket.send(packetSend);
			
			frame.getContentPane().setBackground(Color.GREEN);
			frame.setVisible(true);
				
		}while(cnt!= 10);
		
		socket.close();
		frame.dispose();
		
	}
	
}

package TDM1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

public class Exercice7 {
	public static void main(String[] args) throws IOException {
		Exercice7 ex = new Exercice7();
		ex.jouer();
		
	}
	
	private void jouer() throws IOException
	{
		System.out.println("Demarrage Client ....");
		InetSocketAddress address = new InetSocketAddress("127.0.0.1",11000);
		DatagramSocket socket = new DatagramSocket();
		byte[] receive = new byte[2048];
		DatagramPacket packet = new DatagramPacket(receive,receive.length,address);
		DatagramPacket sendPacket ;
		String message;
		for(int i = 0 ; i <10;i++)
		{
			System.err.println("LANCEMENT PARTIE "+(i+1));
			
		
		byte[] send = "JOUER".getBytes();
		sendPacket= new DatagramPacket(send,send.length,address);
		socket.send(sendPacket);
		/// game start mult 
		do
		{
			socket.receive(packet);
			message = new String(receive,packet.getOffset(),packet.getLength());
			message = message.replace(";", "");
			int a = 0,b = 0,c = 0;
						
			
			if (!message.contains("GAGNE") && !message.contains("PERDU"))
			{

				a = Integer.parseInt(message);
				socket.receive(packet);
				message = new String(receive,packet.getOffset(),packet.getLength());
				message =message.replace(";", "");
				
				b = Integer.parseInt(message);

				c = a*b;
				
				send = (""+c+";").getBytes();
				sendPacket= new DatagramPacket(send,send.length,address);
				socket.send(sendPacket);
				System.err.println("Reception message : "+ a +" et message "+b);
				System.err.println("Envoie message: " + new String(send));
			}
			else 
			{
				send = "".getBytes();
				System.err.println("Reception message : "+ message);				
			}
			

			
			
			
		}while(!message.contains("GAGNE") && !message.contains("PERDU"));
		
		System.err.println("Partie "+(i+1)+" terminé vous avez: "+message);
		
		}
		
		socket.close();
		
	}

}

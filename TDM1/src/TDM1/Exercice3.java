package TDM1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

public class Exercice3 {
	
	public static void main(String[] args) throws IOException {
		Exercice3 ex = new Exercice3();
		ex.execute();
	}
	
	private void execute() throws IOException
	{
		System.out.println("Démarage du client ... ");
		DatagramSocket so = new DatagramSocket();
		InetSocketAddress ard = new InetSocketAddress("192.168.130.178",3000);
		byte [] buff = new String("Hello").getBytes();
		for(int i = 0 ; i < 1000 ; i++)
		{
			
			DatagramPacket packet = new DatagramPacket(buff, buff.length,ard);
			so.send(packet);
			buff = (""+i).getBytes();
		}
		so.close();
		System.out.println("Message envoyé");
	}
	
	

}

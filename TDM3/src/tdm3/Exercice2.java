package tdm3;

import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Exercice2 {
	
	private final String IP = "127.0.0.1";
	private final int PORT = 2000;
	
	
	public static void main(String[] args) throws Exception {
		Exercice2 ex = new Exercice2();
		ex.execute();
	}
	
	
	private void execute() throws Exception 
	{
		System.err.println("Creating  Client ....");
		
		Socket socket = new Socket();
		
		InetSocketAddress address = new InetSocketAddress(IP,PORT);
		socket.connect(address);
		
		byte [] bufferSend  = new String("Un message simple;").getBytes();
		
		OutputStream os = socket.getOutputStream();
		
		os.write(bufferSend);
		
		System.err.println("Client: Send message = "+new String(bufferSend));
		
		socket.close();
		System.err.println("Client: Socket closed ...");
	}

}

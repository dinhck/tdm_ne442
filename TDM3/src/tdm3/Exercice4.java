package tdm3;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Exercice4 {
	
	private final static int PORT = 7500;
	private final static String IP = "127.0.0.1";
	
	
	public static void main(String[] args) throws Exception {
		new Exercice4().execute();
		
	}
	
	private void execute()throws Exception
	{
		System.err.println("Creation Client ...");
		InetSocketAddress address = new InetSocketAddress(IP,PORT);
		Socket socket = new Socket();
		socket.connect(address);
		
		
		byte[] bufferR = new byte[2048];
		byte[] bufferEmit;
		
		
		InputStream receiveStream = socket.getInputStream();
		OutputStream outStream = socket.getOutputStream();
		
		StringBuilder builder = new StringBuilder();
		
		String message = "";
		int bufferLen;
		
		do
		{
			bufferLen = receiveStream.read(bufferR);
			if (bufferLen != -1)
			{
				message = new String(bufferR,0,bufferLen);
				
				System.err.println("Client: Receive DATA = "+message);
				char c;
				for(int i = 0 ; i < bufferLen; i ++)
				{
					c = message.charAt(i);
					builder.append(c);
					if (c == '?')
					{
						System.err.println("Client: Expression complete = "+builder.toString());
						int result =this.compute(builder.toString());
						bufferEmit = (result+";").getBytes();
						System.err.println("Client: Send message = "+result+";");
						outStream.write(bufferEmit);
						builder.setLength(0);
					}
				}	
			}
		}while(!message.contains("STOP") && bufferLen !=-1);
		System.err.println("Client: Close Socket...");
		socket.close();
		
		}
	
	private int compute(String str)
	{
		String [] message = str.split("=");
		message = message[0].split("\\+");
		int a = Integer.parseInt(message[0]);
		int b = Integer.parseInt(message[1]);
		
		return a+b;
	}
		
}

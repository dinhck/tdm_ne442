package tdm3;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JProgressBar;

public class Progress extends JFrame {
	private Thread t;
	private JProgressBar bar;
	private JButton launch;

	public Progress() {
		this.setSize(300, 80);
		this.setTitle("*** File Download ***");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		bar = new JProgressBar();
		bar.setMaximum(500);
		bar.setMinimum(0);
		bar.setStringPainted(true);
		this.getContentPane().add(bar, BorderLayout.CENTER);
		this.setVisible(true);
	}
	
	public JProgressBar getProgressBar()
	{
		return this.bar;
	}
	
}
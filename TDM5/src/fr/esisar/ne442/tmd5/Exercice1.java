package fr.esisar.ne442.tmd5;

public class Exercice1 {

	public  double result = 0d;
	public static void main(String[] args) {
		
		Exercice1 ex = new Exercice1();
		
		for(long i = 0 ; i < 1000000000; i++)
		{
			ex.calcPi(i);
		}
		System.out.println(ex.result*4d);
	}
	
	
	
	public void calcPi( long k)
	{
		double result = 1d/((2d*(double)k)+1d) ;
		if (k%2 == 1)
		{
			result*= -1d;
		}
		
		this.result += result;
	}
}
